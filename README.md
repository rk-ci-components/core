# GitLab CORE CI component

[![Latest Release](https://gitlab.com/rk-ci-components/core/-/badges/release.svg)](https://gitlab.com/rk-ci-components/core/-/releases)
[![pipeline status](https://gitlab.com/rk-ci-components/core/badges/master/pipeline.svg)](https://gitlab.com/rk-ci-components/core/-/commits/master)

This project is a core GitLab CI/CD component used to be included in other components.

It has an adaptive pipeline, some core scripts, default workflow, etc.

## Usage

This template can be used as a [CI/CD component](https://docs.gitlab.com/ee/ci/components/#use-a-component-in-a-cicd-configuration) to be included in other components.

### Use as a CI/CD component

Add the following to your `[template].yml`:

```yaml
include:
  - component: gitlab.com/rk-ci-components/core/core@master
```

### Default workflow
```yaml
workflow:
  rules:
    # prevent branch pipeline when an MR is open (prefer MR pipeline)
    - if: '$CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS'
      when: never
    - when: always
```

### Default test policy
```yaml
# test job prototype: implement adaptive pipeline rules
.test-policy:
  rules:
    # on tag: auto & failing
    - if: $CI_COMMIT_TAG
    # on ADAPTIVE_PIPELINE_DISABLED: auto & failing
    - if: '$ADAPTIVE_PIPELINE_DISABLED == "true"'
    # on production or integration branch(es): auto & failing
    - if: '$CI_COMMIT_REF_NAME =~ $PROD_REF || $CI_COMMIT_REF_NAME =~ $INTEG_REF'
    # early stage (dev branch, no MR): manual & non-failing
    - if: '$CI_MERGE_REQUEST_ID == null && $CI_OPEN_MERGE_REQUESTS == null'
      when: manual
      allow_failure: true
    # Draft MR: auto & non-failing
    - if: '$CI_MERGE_REQUEST_TITLE =~ /^Draft:.*/'
      allow_failure: true
    # else (Ready MR): auto & failing
    - when: on_success
```

This can be used as follows:

```yaml
rules:
  - !reference [ .test-policy, rules ]
```

### Default variables
```yaml
variables:
  # default production ref name (pattern)
  PROD_REF: /^(master|main)$/
  # default integration ref name (pattern)
  INTEG_REF: /^develop$/
```

These can be used like:

```yaml
rules:
  - if: $CI_COMMIT_REF_NAME =~ $PROD_REF
    when: always
```

### Default stages
```yaml
stages:
  - build
  - test
  - package-build
  - package-test
  - infra
  - deploy
  - acceptance
  - publish
  - infra-prod
  - production
```

### Default scripts
* **log_info** - *Log an information message*
* **log_success** - *Log a success message*
* **log_warn** - *Log a warning message*
* **log_error** - *Log an error message*
* **to_ssc** - *Converts a string to SCREAMING_SNAKE_CASE*
* **awkenvsubst** - *Performs variables escaping: '&' for gsub + JSON chars ('\' and '"')*
* **assert_defined** - *Asserts a variable*

These scripts can be included like so

```yaml
before_script:
  - !reference [.core-scripts]
```